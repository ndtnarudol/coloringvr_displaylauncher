﻿using SmartLocalization;
using System;
using System.IO;
using System.Diagnostics;
using UnityEngine;
using System.Collections.Generic;
using UIWidgets;
using UnityEngine.UI;

#if !UNITY_EDITOR
using System.Windows.Forms;
#else
using UnityEditor;
#endif

public class UIController : Singleton<UIController>
{
	//-----------------------------------------------------------------------------------

    [SerializeField] private Canvas                 m_Canvas;

    [Header("Resolution")]
    [SerializeField] private InputField             m_InputScreenPosX;
    [SerializeField] private InputField             m_InputScreenPosY;
    [SerializeField] private InputField             m_InputScreenWidth;
    [SerializeField] private InputField             m_InputScreenHeight;

    [Header("Settings")]
    [SerializeField] private Toggle                 m_ToggleBorderLess;
    [SerializeField] private Toggle                 m_ToggleMoveable;

    [Header("Extension")]
    [SerializeField] private UIWidgets.ListView     m_ExtensionListView;

    [Header("Path")]
    [SerializeField] private InputField             m_PathImage;
    [SerializeField] private InputField             m_ApplicationPath;

    //-----------------------------------------------------------------------------------

    [Header("Popup")]
    [SerializeField] private Popup                  m_PopupTemplate;
    [SerializeField] private Dialog                 m_DialogTemplate;

    //-----------------------------------------------------------------------------------

    [Header("Picker")]
    [SerializeField] private PickerIcons            m_ThemePickerTemplate;
    [SerializeField] private PickerIcons            m_LanguagePickerTemplate;

    //-----------------------------------------------------------------------------------
    
    private PickerIcons                             m_LanguagePicker;
    private PickerIcons                             m_ThemePickerPicker;
    private Popup                                   m_Popup;
    private Dialog                                  m_Dialog;
    private Config                                  m_Config;
    private ConfigWriter                            m_ConfigWriter;
    private ConfigReader                            m_ConfigReader;

	//-----------------------------------------------------------------------------------

	protected override void AfterAwake ()
    {
        m_ConfigWriter          =   new ConfigWriter();
        m_ConfigReader          =   new ConfigReader();

	    m_Popup                 =   m_PopupTemplate.Template();
	    m_Dialog                =   m_DialogTemplate.Template();
        m_LanguagePicker        =   m_LanguagePickerTemplate.Template();
        m_ThemePickerPicker     =   m_ThemePickerTemplate.Template();

	    LoadConfigFromFile();
    }

	//-----------------------------------------------------------------------------------

    public void ShowWarningPopup(string _title, string _message)
    {
        m_Popup.Show
        (
            title: _title,
            message: _message,
            modal:true,
            modalColor:new Color(0f, 0f, 0f, 100f/255f),
            canvas:m_Canvas
        );
    }

	//-----------------------------------------------------------------------------------

    public void ShowThemePicker()
    {
        m_ThemePickerPicker.Show
        (
            defaultValue:m_ThemePickerPicker.ListView.SelectedItem,
            onSelect: (_item) =>
            {
                switch (_item.Value)
                {
                    default:
                        break;
                }
            },
            modalColor:new Color(0f, 0f, 0f, .8f), 
			canvas: m_Canvas
        );
    }

	//-----------------------------------------------------------------------------------

    public void ShowLanguagePicker()
    {
        m_LanguagePicker.Show
        (
            defaultValue:m_LanguagePicker.ListView.SelectedItem,
            onSelect: (_item) =>
            {
                switch (_item.Value)
                {
                    case 0:

                        LanguageManager.Instance.ChangeLanguage("en");
                        break;

                    case 1:

                        LanguageManager.Instance.ChangeLanguage("ja");
                        break;

                    default:
                        break;
                }
            },
            modalColor:new Color(0f, 0f, 0f, .8f), 
			canvas: m_Canvas
        );
    }

	//-----------------------------------------------------------------------------------

    public void ShowWarningDialog(string _title, string _message, Func<bool> _onClickPositive)
    {
        m_Dialog.Show
        (
            title: _title,
            message: _message,
            buttons:new DialogActions()
            {
				{LanguageManager.Instance.GetTextValue("Global.Yes"), _onClickPositive},
				{LanguageManager.Instance.GetTextValue("Global.No"), Dialog.Close},
			},
            modal:true,
            modalColor:new Color(0f, 0f, 0f, 100f/255f),
            canvas:m_Canvas
        );
    }

	//-----------------------------------------------------------------------------------

    private void LoadConfigFromUI()
    {
        #region Resolution

        int.TryParse(m_InputScreenPosX.text,    out m_Config.screenPosX);
        int.TryParse(m_InputScreenPosY.text,    out m_Config.screenPosY);
        int.TryParse(m_InputScreenWidth.text,   out m_Config.screenWidth);
        int.TryParse(m_InputScreenHeight.text,  out m_Config.screenHeight);

        #endregion

        #region Settings

        m_Config.borderless     =   m_ToggleBorderLess.isOn;
        m_Config.moveable       =   m_ToggleMoveable.isOn;

        #endregion

        #region Extensions

        m_Config.extension = GetExtensions().ToArray();

        #endregion

        #region Path

        m_Config.applicationPath    =   m_ApplicationPath.text;
        m_Config.imagePath          =   m_PathImage.text;

        #endregion
    }

	//-----------------------------------------------------------------------------------

    private void LoadConfigFromFile()
    {
        m_Config        =   m_ConfigReader.GetConfig();

        #region Resolution

        m_InputScreenPosX.text      =   m_Config.screenPosX.ToString();
        m_InputScreenPosY.text      =   m_Config.screenPosY.ToString();
        m_InputScreenWidth.text     =   m_Config.screenWidth.ToString();
        m_InputScreenHeight.text    =   m_Config.screenHeight.ToString();

        #endregion

        #region Settings

        m_ToggleBorderLess.isOn =   m_Config.borderless;
        m_ToggleMoveable.isOn   =   m_Config.moveable;

        #endregion

        #region Extensions

        var extensions = new List<string>(m_Config.extension);

        if (extensions.Contains(".png"))
            m_ExtensionListView.Select(0);

        if (extensions.Contains(".jpg"))
            m_ExtensionListView.Select(1);

        if (extensions.Contains(".jpeg"))
            m_ExtensionListView.Select(2);

        #endregion

        #region Path

        m_ApplicationPath.text  =   m_Config.applicationPath;
        m_PathImage.text        =   m_Config.imagePath;

        #endregion
    }

    //-----------------------------------------------------------------------------------

    private List<string> GetExtensions()
    {
        var result  =   new List<string>();

        for (var i = 0; i < m_ExtensionListView.DataSource.Count; i++)
	    {
	        if (m_ExtensionListView.IsSelected(i))
	        {
	            switch (i)
	            {
                    case 0:
	                    result.Add(".png");
	                    break;
                    case 1:
	                    result.Add(".jpg");
	                    break;
                    case 2:
	                    result.Add(".jpeg");
	                    break;
	            }
	        }
	    }

        return result;
    }

	//-----------------------------------------------------------------------------------

    public void BrowseFile(InputField _input)
    {
        string path = string.Empty;

#if UNITY_EDITOR

        path    =   EditorUtility.OpenFilePanelWithFilters
                    (
                        LanguageManager.Instance.GetTextValue("Dialog.Application.Title"),
                        _input.text,
                        new []{"Executeable", "exe"}
                    );

#else

        OpenFileDialog openFileDialog1 = new OpenFileDialog();

		openFileDialog1.Title = LanguageManager.Instance.GetTextValue("Dialog.Application.Title");
		openFileDialog1.InitialDirectory = _input.text ;
		openFileDialog1.Filter ="Executeable (*.exe)|*.exe";
		openFileDialog1.FilterIndex = 2 ;
		openFileDialog1.RestoreDirectory = true ;
        
		if(openFileDialog1.ShowDialog() == DialogResult.OK)
			_input.text	=	openFileDialog1.FileName;

#endif

        if (path.Length > 0)
        {
            _input.text = path.Replace("/", "\\"); ;

        }
    }
	//-----------------------------------------------------------------------------------

    public void BrowsePath(InputField _input)
    {
        string path = string.Empty;

#if UNITY_EDITOR

        path = EditorUtility.OpenFolderPanel
                            (
                                LanguageManager.Instance.GetTextValue("Dialog.Folder.Title"),
                                _input.text,
                                ""
                            );

#else

		FolderBrowserDialog fbd = new FolderBrowserDialog();

		fbd.ShowDialog();

		if (!string.IsNullOrEmpty(fbd.SelectedPath))
			path	=	fbd.SelectedPath;

#endif

        if (path.Length > 0)
        {
            _input.text = path.Replace("/", "\\"); ;

        }
    }

    //-----------------------------------------------------------------------------------

    public void ClickSave()
    {
        var ipExist = Directory.Exists(m_PathImage.text);
        var apExist = File.Exists(m_ApplicationPath.text);

        if (!ipExist || !apExist)
        {
            ShowWarningDialog
            (
                LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath"),
                (
                    !ipExist && !apExist ?
                    LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath.Both"):
                    !apExist ?
                    LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath.Application"):
                    LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath.Folder")
                ) + LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath.Confirm"),
                () =>
                {
                    Save();
                    return true;
                }
            );
        }
        else
        {
            Save();
        }
    }

    //-----------------------------------------------------------------------------------

    public void Save()
    {
        LoadConfigFromUI();
        m_ConfigWriter.WriteConfig(m_Config);
    }

	//-----------------------------------------------------------------------------------

    public void Reset()
    {
        ConfigWriter.WriteDefaultConfig();
        LoadConfigFromFile();
    }

	//-----------------------------------------------------------------------------------

    public void ClickLaunch()
    {
        if (!File.Exists(m_ApplicationPath.text))
        {
            ShowWarningPopup
            (
                LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath"),
                LanguageManager.Instance.GetTextValue("Dialog.Error.InvalidPath.Application")
            );
        }
        else
        {
            Launch();
        }
    }

	//-----------------------------------------------------------------------------------

    public void Launch()
    {
		var processes = GetProcesses(m_ApplicationPath.text);

		if(processes.Length <= 0)
		{
			LaunchApplication();
		}
		else if (processes.Length > 0)
		{
		    CloseDisplay();
			LaunchApplication();
		}
    }

	//-----------------------------------------------------------------------------------

    private void LaunchApplication()
    {
        var startInfo = new ProcessStartInfo(m_ApplicationPath.text)
        {
            UseShellExecute = false,
            CreateNoWindow = true,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            RedirectStandardInput = true
        };

        var process = new Process {StartInfo = startInfo};
        process.Start();
    }

	//-----------------------------------------------------------------------------------

    private Process[] GetProcesses(string _fullpath)
    {
        return Process.GetProcessesByName(Path.GetFileNameWithoutExtension(_fullpath));
    }

	//-----------------------------------------------------------------------------------

    public void CloseDisplay()
    {
		var processes = GetProcesses(m_ApplicationPath.text);

        foreach (var process in processes)
		{
			process.Kill();
			process.WaitForExit();
		}
    }

	//-----------------------------------------------------------------------------------

    public void CloseLauncher()
    {
        UnityEngine.Application.Quit();
    }

	//-----------------------------------------------------------------------------------
}
