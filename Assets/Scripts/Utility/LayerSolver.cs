﻿using UnityEngine;

namespace Mizore.Utility
{
    public class LayerSolver
    {
		//-------------------------------------------------------------------------------

        public static void SetLayer(GameObject go, int layer)
        {
            if (go == null) return;
            foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
            {
                trans.gameObject.layer = layer;
            }
        }

        //-------------------------------------------------------------------------------
    }
}

