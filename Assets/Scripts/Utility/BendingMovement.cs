using UnityEngine;

namespace Mizore.Utility
{
    public static class BendingMovement
    {
        //------------------------------------------------------------------

        public static Vector3 MoveToPosition(Vector3 start, Vector3 end, Vector3 bending, float time)
        {
			
			var result = Vector3.Lerp(start, end, time);

			result.x += bending.x * Mathf.Sin(time * Mathf.PI);
			result.y += bending.y * Mathf.Sin(time * Mathf.PI);
			result.z += bending.z * Mathf.Sin(time * Mathf.PI);

            return result;
        }
		
        //------------------------------------------------------------------

        public static Vector3 EaseBackInToPosition(Vector3 start, Vector3 end, Vector3 bending, float time)
        {
			var	result	=	new Vector3
							(
								Easing.BackEaseIn
								(
									time,
									start.x,
									end.x,
									1f
								),
								Easing.BackEaseIn
								(
									time,
									start.y,
									end.y,
									1f
								),
								Easing.BackEaseIn
								(
									time,
									start.z,
									end.z,
									1f
								)
							);

			result.x += bending.x * Mathf.Sin(time * Mathf.PI);
			result.y += bending.y * Mathf.Sin(time * Mathf.PI);
			result.z += bending.z * Mathf.Sin(time * Mathf.PI);

            return result;
        }

        //------------------------------------------------------------------
    }
}