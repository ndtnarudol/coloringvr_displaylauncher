﻿using UnityEngine;

public class RenderqueueSetting : MonoBehaviour
{
    //-----------------------------------------------------------------
    
    public int renderQueue  =   2002;
    
    //-----------------------------------------------------------------

    void Start()
    {
        var renders = GetComponentsInChildren<Renderer>();
        
        foreach (var rendr in renders)
        {
            rendr.material.renderQueue = renderQueue; 
        }
    }
    
    //-----------------------------------------------------------------
}
