﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class PathWatcher : Singleton<PathWatcher>
{
	//----------------------------------------------------

	[SerializeField] string		m_WatchingPath;
	[SerializeField] string		m_WatchingFilter;

	//----------------------------------------------------

	public static System.IO.FileSystemEventHandler	onCreated;
	public static System.IO.FileSystemEventHandler	onChanged;
	public static System.IO.FileSystemEventHandler	onDeleted;

	//----------------------------------------------------

	private FileSystemWatcher	m_Watcher;

	//----------------------------------------------------

	public void Init (string _watchingPath) 
	{
		m_WatchingPath	=	_watchingPath;

		CreateWatcher();
		Debug.Log(string.Format("Initializing FileSystemWatcher at '{0}'", m_WatchingPath));
	}
	
	//----------------------------------------------------

	private void CreateWatcher()
	{
		DestroyWatcher();

		if(!System.IO.Directory.Exists(m_WatchingPath))
		{
			Debug.LogError(string.Format("Path {0} not exist", m_WatchingPath));
			return;
		}

		m_Watcher		=	new FileSystemWatcher(m_WatchingPath, m_WatchingFilter);
		m_Watcher.EnableRaisingEvents = true;
		
		m_Watcher.Created	+=	onCreated;
		m_Watcher.Changed	+=	onChanged;
		m_Watcher.Deleted	+=	onDeleted;
	}

	//----------------------------------------------------

	private void DestroyWatcher()
	{
		if(m_Watcher != null)
		{
			m_Watcher.Created	-=	onCreated;
			m_Watcher.Changed	-=	onChanged;
			m_Watcher.Deleted	-=	onDeleted;
			m_Watcher.Dispose();
		}
	}

	//----------------------------------------------------

	private void OnEnable()
	{
		CreateWatcher();
	}
	
	//----------------------------------------------------

	private void OnDisable()
	{
		DestroyWatcher();
	}

	//----------------------------------------------------
}
