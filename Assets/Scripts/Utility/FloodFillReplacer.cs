﻿using UnityEngine;
using System.Collections.Generic;

public class FloodFillReplacer 
{
	//------------------------------------------------------------------------------------

	public enum FillAlgorithm
	{
		FourDirection	=	0,
		EightDirection	=	1
	}
	//-----------------------------------------------------------------

	private struct PixelPos
	{
		public int x;
		public int y;
		public int index;

		public PixelPos(int _x, int _y, int _index)
		{
			x		=	_x;
			y		=	_y;
			index	=	_index;
		}
	}

	//------------------------------------------------------------------------------------
	
	public static void ReplacePixel 
							(
								Texture2D _sourceTexture,
								ref Texture2D _texture,
								Color32 _baseColor,
								Color32 _replaceColor,
								float _threshold = .1f
							) 
	{

		var	pixels			=	_texture.GetPixels32();
		
		for(int i = 0; i < pixels.Length; i++)
		{
			if (ColorWithinThreshold(_baseColor, pixels[i], _threshold)) 
				pixels[i]	=	_replaceColor;
		}

		_texture.SetPixels32(pixels);
		_texture.Apply();
	}

	//------------------------------------------------------------------------------------
	
	public static void ReplacePixel 
							(
								ref Texture2D _texture,
								Color32 _baseColor,
								Color32 _replaceColor,
								float _threshold = .1f
							) 
	{

		var	pixels			=	_texture.GetPixels32();
		
		for(int i = 0; i < pixels.Length; i++)
		{
			if (ColorWithinThreshold(_baseColor, pixels[i], _threshold)) 
				pixels[i]	=	_replaceColor;
		}

		_texture.SetPixels32(pixels);
		_texture.Apply();
	}

	//------------------------------------------------------------------------------------
	
	public static Texture2D ReplacePixel 
							(
								Texture2D _texture,
								Color32 _baseColor,
								Color32 _replaceColor,
								float _threshold = .1f
							) 
	{
		var	result			=	new Texture2D
								(
									_texture.width,
									_texture.height,
									_texture.format,
									false
								);

		var	pixels			=	_texture.GetPixels32();
		
		for(int i = 0; i < pixels.Length; i++)
		{
			if (ColorWithinThreshold(_baseColor, pixels[i], _threshold)) 
				pixels[i]	=	_replaceColor;
		}

		result.SetPixels32(pixels);
		result.Apply();

		return result;
	}

	//------------------------------------------------------------------------------------
	
	public static void 	ReplacePixel 
						(
							ref Texture2D _texture, 
							int _x, 
							int _y, 
							Color32 _replaceColor,
							float _threshold = .1f,
							FillAlgorithm _algorithm = FillAlgorithm.FourDirection
						) 
	{
		var	pixels			=	_texture.GetPixels32();
		var baseColor		=	pixels[GetIndexCoordinate(_x, _y, _texture.width)];
		var targetPixels	=	GetSameColorAroundPixel
								(
									_x,
									_y,
									_texture.width,
									_texture.height,
									_threshold,
									baseColor,
									pixels,
									_algorithm
								);

		for(int i = 0; i < targetPixels.Count; i++)
			pixels[targetPixels[i]]	=	_replaceColor;

		_texture.SetPixels32(pixels);
		_texture.Apply();
	}

	//------------------------------------------------------------------------------------
	
	public static Texture2D ReplacePixel 
							(
								Texture2D _texture, 
								int _x, 
								int _y, 
								Color32 _replaceColor,
								float _threshold = .1f,
								FillAlgorithm _algorithm = FillAlgorithm.FourDirection
							) 
	{
		var	result			=	new Texture2D
								(
									_texture.width,
									_texture.height,
									_texture.format,
									false
								);

		var	pixels			=	_texture.GetPixels32();
		var baseColor		=	pixels[GetIndexCoordinate(_x, _y, _texture.width)];
		var targetPixels	=	GetSameColorAroundPixel
								(
									_x,
									_y,
									_texture.width,
									_texture.height,
									_threshold,
									baseColor,
									pixels,
									_algorithm
								);

		for(int i = 0; i < targetPixels.Count; i++)
			pixels[targetPixels[i]]	=	_replaceColor;

		result.SetPixels32(pixels);
		result.Apply();

		return result;
	}

	//-----------------------------------------------------------------
	
	private static List<int> GetSameColorAroundPixel(int _x, int _y, int _width, int _height, float _threshold, Color32 _baseColor, Color32[] _pixels, FillAlgorithm _algorithm)
	{
		var processedIndex	=	new Dictionary<int, bool>();
		var	processedQueue	=	new Queue<PixelPos>();

		var	result		=	new List<int>();
		var currentPos	=	new PixelPos(_x, _y, GetIndexCoordinate(_x, _y, _width));

		processedQueue.Enqueue(currentPos);
		processedIndex.Add(currentPos.index, true);

		while (processedQueue.Count > 0)
		{
			var	dequeuedPos		=	processedQueue.Dequeue();
			
			if(!ColorWithinThreshold(_baseColor, _pixels[dequeuedPos.index], _threshold))
				continue;

			result.Add(dequeuedPos.index);
			
			var	upNode		=	new PixelPos(dequeuedPos.x, dequeuedPos.y+1, GetIndexCoordinate(dequeuedPos.x, dequeuedPos.y+1, _width));
			var	downNode	=	new PixelPos(dequeuedPos.x, dequeuedPos.y-1, GetIndexCoordinate(dequeuedPos.x, dequeuedPos.y-1, _width));
			var	leftNode	=	new PixelPos(dequeuedPos.x-1, dequeuedPos.y, GetIndexCoordinate(dequeuedPos.x-1, dequeuedPos.y, _width));
			var	rightNode	=	new PixelPos(dequeuedPos.x+1, dequeuedPos.y, GetIndexCoordinate(dequeuedPos.x+1, dequeuedPos.y, _width));
			
			if(dequeuedPos.x - 1 >= 0 && !processedIndex.ContainsKey(leftNode.index))
			{
				processedQueue.Enqueue(leftNode);
				processedIndex.Add(leftNode.index, true);
			}
			
			if(dequeuedPos.x + 1 < _width && !processedIndex.ContainsKey(rightNode.index))
			{
				processedQueue.Enqueue(rightNode);
				processedIndex.Add(rightNode.index, true);
			}
			
			if(dequeuedPos.y - 1 >= 0 && !processedIndex.ContainsKey(downNode.index))
			{
				processedQueue.Enqueue(downNode);
				processedIndex.Add(downNode.index, true);
			}
			
			if(dequeuedPos.y + 1 < _height && !processedIndex.ContainsKey(upNode.index))
			{
				processedQueue.Enqueue(upNode);
				processedIndex.Add(upNode.index, true);
			}

			if(_algorithm.Equals(FillAlgorithm.EightDirection))
			{
				var	topLeftNode		=	new PixelPos(dequeuedPos.x-1, dequeuedPos.y+1, GetIndexCoordinate(dequeuedPos.x-1, dequeuedPos.y+1, _width));
				var	topRightNode	=	new PixelPos(dequeuedPos.x+1, dequeuedPos.y+1, GetIndexCoordinate(dequeuedPos.x+1, dequeuedPos.y+1, _width));
				var	bottomLeftNode	=	new PixelPos(dequeuedPos.x-1, dequeuedPos.y-1, GetIndexCoordinate(dequeuedPos.x-1, dequeuedPos.y-1, _width));
				var	bottomRightNode	=	new PixelPos(dequeuedPos.x+1, dequeuedPos.y-1, GetIndexCoordinate(dequeuedPos.x+1, dequeuedPos.y-1, _width));

				if(dequeuedPos.x - 1 >= 0 && dequeuedPos.y + 1 < _height && !processedIndex.ContainsKey(topLeftNode.index))
				{
					processedQueue.Enqueue(topLeftNode);
					processedIndex.Add(topLeftNode.index, true);
				}
				
				if(dequeuedPos.x + 1 < _width && dequeuedPos.y + 1 < _height  && !processedIndex.ContainsKey(topRightNode.index))
				{
					processedQueue.Enqueue(topRightNode);
					processedIndex.Add(topRightNode.index, true);
				}
				
				if(dequeuedPos.x - 1 >= 0 && dequeuedPos.y - 1 >= 0 && !processedIndex.ContainsKey(bottomLeftNode.index))
				{
					processedQueue.Enqueue(bottomLeftNode);
					processedIndex.Add(bottomLeftNode.index, true);
				}
				
				if(dequeuedPos.x + 1 < _width && dequeuedPos.y - 1 >= 0 && !processedIndex.ContainsKey(bottomRightNode.index))
				{
					processedQueue.Enqueue(bottomRightNode);
					processedIndex.Add(bottomRightNode.index, true);
				}
			}
		}

		return result;
	}

	//-----------------------------------------------------------------

	private static bool ColorWithinThreshold(Color32 _targetColor, Color32 _inputColor, float _threshold)
	{
		var	targetGrayscale	=	GrayscaleColor(_targetColor) / byte.MaxValue;
		var	inputGrayscale	=	GrayscaleColor(_inputColor) / byte.MaxValue;

		float max = Mathf.Clamp01(targetGrayscale + _threshold);
		float min = Mathf.Clamp01(targetGrayscale - _threshold);

		return inputGrayscale >= min && inputGrayscale <= max;
	}

	//-----------------------------------------------------------------

	private static float GrayscaleColor(Color32 _color)
	{
		return 0.2126f * _color.r + 0.7152f * _color.g + 0.0722f * _color.b;
	}

	//-----------------------------------------------------------------

	private static Vector2 GetPixelCoordinate(int _index, int _width)
	{
		var	_y	=	 _index / _width;
		var	_x	=	_index % _width;

		return new Vector2(_x, _y);
	}

	//-----------------------------------------------------------------

	private static int GetIndexCoordinate(int _x, int _y, int _width)
	{
		return _x + _width * _y;
	}

	//------------------------------------------------------------------------------------
}
