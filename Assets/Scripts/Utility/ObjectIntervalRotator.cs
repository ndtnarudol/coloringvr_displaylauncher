﻿using UnityEngine;
using System.Collections;

public class ObjectIntervalRotator : MonoBehaviour
{
	//---------------------------------------------------------
	
	public	float	updateInterval		=	1f;
	public	Vector3	rotateAmount		=	new Vector3(0f, 0f, 45f);
	
	//---------------------------------------------------------
	
	private void Start()
	{
		StartCoroutine("RotateCoroutine");
	}

	//---------------------------------------------------------
	
	private IEnumerator RotateCoroutine()
	{
		while(true)
		{
			yield return new WaitForSeconds(updateInterval);
			
			transform.Rotate(rotateAmount);
		}
	}
	
	//---------------------------------------------------------
}
