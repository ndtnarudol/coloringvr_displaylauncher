using UnityEngine;

public class TextureConverter
{

	//-----------------------------------------------------------------
	
	public static Sprite ByteToSprite(byte[] bytes)
	{
		return Texture2DToSprite(ByteToTexture2D(bytes));
	}
	
	//-----------------------------------------------------------------
	
	public static Texture2D ByteToTexture2D(byte[] bytes)
	{
		var texture  =   new Texture2D( 0, 0, TextureFormat.RGBA32, false );
		texture.LoadImage( bytes );
		
		return texture;
	}

	//-----------------------------------------------------------------
	
	public static Texture2D SpriteToTexture2D(Sprite sprite)
	{
		var texture  =   new Texture2D
						( 
							Mathf.RoundToInt(sprite.rect.width), 
							Mathf.RoundToInt(sprite.rect.height), 
							TextureFormat.RGBA32, 
							false 
						);
		texture.LoadImage(sprite.texture.EncodeToPNG());
		texture.Apply();
			
		return texture;
	}

	//-----------------------------------------------------------------
	
	public static Sprite Texture2DToSprite(Texture2D texture)
	{
		var spriteRect  =   new Rect(0, 0, texture.width, texture.height);
		var sprite      =   Sprite.Create(texture, spriteRect, new Vector2(.5f, .5f));
		
		return sprite;
	}
	
	//-----------------------------------------------------------------
}
