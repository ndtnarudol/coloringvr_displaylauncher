﻿using UnityEngine;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class ConstrainPoint : MonoBehaviour, IDragHandler
{
	//-----------------------------------------------------------------

	[Header("Limit Canvas Area")]
	[SerializeField]	private	RectTransform	m_LimitArea;
	[SerializeField]	private	RectTransform	m_TopLeft;
	[SerializeField]	private	RectTransform	m_BottomRight;

	[Header("Other Corners")]
	[SerializeField]	private	RectTransform	m_OtherHorizontalConstrain;
	[SerializeField]	private	RectTransform	m_OtherVerticalConstrain;

	//-----------------------------------------------------------------

	private	Vector3		m_DefaultPosition;

	//-----------------------------------------------------------------

	private void Start()
	{
		m_DefaultPosition	=	transform.position;
	}

	//-----------------------------------------------------------------
	
	public void OnDrag(PointerEventData _eventData)
	{
		Vector2	localCursor;

		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(m_LimitArea, _eventData.position, _eventData.pressEventCamera, out localCursor))
			return;

		if(m_OtherHorizontalConstrain == null || m_OtherHorizontalConstrain == null)
			return;

		var	desiredX	=	_eventData.position.x;
		var	desiredY	=	_eventData.position.y;

		if(_eventData.position.x < m_TopLeft.position.x)
		{
			desiredX	=	m_TopLeft.position.x;	
		}
		if(_eventData.position.y > m_TopLeft.position.y)
		{
			desiredY	=	m_TopLeft.position.y;
		}
		if(_eventData.position.x > m_BottomRight.position.x)
		{
			desiredX	=	m_BottomRight.position.x;
		}
		if(_eventData.position.y < m_BottomRight.position.y)
		{
			desiredY	=	m_BottomRight.position.y;
		}

		transform.position					=	new Vector2(desiredX, desiredY);

		m_OtherHorizontalConstrain.position	=	new Vector3	
												(
													transform.position.x,
													m_OtherHorizontalConstrain.position.y,
													m_OtherHorizontalConstrain.position.z
												);
												
		m_OtherVerticalConstrain.position	=	new Vector3	
												(
													m_OtherVerticalConstrain.position.x,
													transform.position.y,
													m_OtherVerticalConstrain.position.z
												);
	}

	//-----------------------------------------------------------------

	public void Reset()
	{
		transform.position	=	m_DefaultPosition;
	}

	//-----------------------------------------------------------------
}