﻿using UnityEngine;

public class ScreenToUIRectSpace 
{

	//-----------------------------------------------------------------

	public static Vector2 ScreenToUIRect (Vector2 _screenPosition, Vector2 _topLeftRectPosition, Vector2 _rectSize) 
	{
		var screenRatio	=	new Vector2
							(
								_screenPosition.x / Screen.width,
								_screenPosition.y / Screen.height
							);

		return 	new Vector2
				(
					_topLeftRectPosition.x +_rectSize.x * screenRatio.x,
					_topLeftRectPosition.y -_rectSize.y * screenRatio.y
				);
	}

	//-----------------------------------------------------------------
}
