﻿using UnityEngine;

public class GarbageCollector : MonoBehaviour
{
	//------------------------------------------------------------------
	
	public int	frameAmount		=	120;
	
	//------------------------------------------------------------------
	
	private void LateUpdate()
	{
		if(Time.frameCount % frameAmount == 0)
		{
			System.GC.Collect();
		}
	}
	
	//------------------------------------------------------------------
}
