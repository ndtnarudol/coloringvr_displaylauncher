﻿using UnityEngine;

public class CornerConstrain : MonoBehaviour 
{
	//-----------------------------------------------------------------

	[Header("Constrain Area")]
	[SerializeField]	private	RectTransform	m_ConstrainArea;

	//-----------------------------------------------------------------

	[Header("Corners")]
	[SerializeField]	private	RectTransform	m_TopLeftCorner;
	[SerializeField]	private	RectTransform	m_TopRightCorner;
	[SerializeField]	private	RectTransform	m_BottomLeftCorner;
	[SerializeField]	private	RectTransform	m_BottomRightCorner;

	//-----------------------------------------------------------------

	public void Init(Vector2 _tlPos, Vector2 _trPos, Vector2 _blPos, Vector2 _brPos)
	{
		m_TopLeftCorner.anchoredPosition		=	_tlPos;
		m_TopRightCorner.anchoredPosition		=	_trPos;
		m_BottomLeftCorner.anchoredPosition		=	_blPos;
		m_BottomRightCorner.anchoredPosition	=	_brPos;
	}

	//-----------------------------------------------------------------

	public void GetCropArea(RectTransform _rect, Texture2D _texture, out int _x, out int _y, out int _width, out int _height)
	{
		int _topX, _topY;

		// Get top left on texture's cordinate
		RectTransformHelper.GetClickedPixel
		(
			m_BottomLeftCorner.position,
			null,
			_rect,
			_texture,
			out _x,
			out _y
		);

		// Get bottom right on texture's cordinate
		RectTransformHelper.GetClickedPixel
		(
			m_TopRightCorner.position,
			null,
			_rect,
			_texture,
			out _topX,
			out _topY
		);

		_width	=	_topX - _x;
		_height	=	_topY - _y;

		Debug.Log(string.Format("X:{0}, Y:{1}, Width:{2}, Height:{3}", _x, _y, _width, _height));
	}

	//-----------------------------------------------------------------

	private void LateUpdate () 
	{	
		if
		(
			m_TopLeftCorner.position.x > m_TopRightCorner.position.x ||
			m_BottomLeftCorner.position.x > m_BottomRightCorner.position.x
		)
		{
			m_TopRightCorner.position		=	new Vector3
												(
													m_TopLeftCorner.position.x,
													m_TopRightCorner.position.y,
													m_TopRightCorner.position.z
												);
			m_BottomRightCorner.position	=	new Vector3
												(
													m_BottomLeftCorner.position.x,
													m_BottomRightCorner.position.y,
													m_BottomRightCorner.position.z
												);	
		}

		if
		(
			m_TopLeftCorner.position.y < m_BottomLeftCorner.position.y ||
			m_TopRightCorner.position.y < m_BottomRightCorner.position.y
		)
		{
			m_TopRightCorner.position		=	new Vector3
												(
													m_TopRightCorner.position.x,
													m_BottomRightCorner.position.y,
													m_TopRightCorner.position.z
												);
			m_TopLeftCorner.position		=	new Vector3
												(
													m_TopLeftCorner.position.x,
													m_BottomLeftCorner.position.y,
													m_TopLeftCorner.position.z
												);	
		}

		m_ConstrainArea.position	=	m_TopLeftCorner.GetComponent<RectTransform>().position;
		m_ConstrainArea.sizeDelta	=	new Vector2
										(
											m_BottomRightCorner.GetComponent<RectTransform>().anchoredPosition.x - m_TopLeftCorner.GetComponent<RectTransform>().anchoredPosition.x,
											m_TopLeftCorner.GetComponent<RectTransform>().anchoredPosition.y - m_BottomRightCorner.GetComponent<RectTransform>().anchoredPosition.y
										);
	}

	//-----------------------------------------------------------------
}