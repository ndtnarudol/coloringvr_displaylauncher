using UnityEngine;

public class TextureRotator
{
	//-------------------------------------------------------------------------------
	
	public static Texture2D RotateLeft(Texture2D texture)
	{
		return RotateTexture2D(texture);
	}

	//-------------------------------------------------------------------------------
	
	public static Texture2D RotateUpsideDown(Texture2D texture)
	{
		var result		=	new Texture2D
							(
								texture.width, 
								texture.height
							);
		var pixels		=	texture.GetPixels32();
		System.Array.Reverse(pixels);
		result.SetPixels32(pixels);
		result.Apply();

		return result;
	}

	//-------------------------------------------------------------------------------
	
	public static Texture2D RotateRight(Texture2D texture)
	{
		var rotateLeft	=	RotateTexture2D(texture);
		return RotateUpsideDown(rotateLeft);
	}
	
	//-------------------------------------------------------------------------------

	private static Texture2D RotateTexture2D(Texture2D texture)
	{
		var result	=	new Texture2D
						(
							texture.height, 
							texture.width
						);
		var pixels	=	texture.GetPixels32();
		pixels		=	RotateMatrix(pixels, texture.width, texture.height);
		result.SetPixels32(pixels);
		result.Apply();

		return result;
	}
	
	//-------------------------------------------------------------------------------
	
	public static Color32[] RotateMatrix(Color32[] tex, int wid, int hi)
	{
		Color32[] ret = new Color32[wid * hi];      //reminder we are flipping these in the target

		for (int y = 0; y < hi; y++)
		{
			for (int x = 0; x < wid; x++)
			{
				ret[(hi-1)-y + x * hi] = tex[x + y * wid];         //juggle the pixels around
				
			}
		}

		return ret;
	}

	//-------------------------------------------------------------------------------
}