using UnityEngine;

public class RectTransformHelper
{
	//-----------------------------------------------------------------

	public static bool GetClickedPosition(Vector3 _position, Camera _camera, RectTransform _rectArea, out int _x, out int _y)
	{
		Vector2 localCursor;

		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectArea.GetComponent<RectTransform>(), _position, _camera, out localCursor))
		{
			_x	=	_y	=	0;
			return	false;
		}

		_x	=	Mathf.RoundToInt(localCursor.x);
		_y	=	Mathf.RoundToInt(_rectArea.rect.height+localCursor.y);
		return true;
	}
	//-----------------------------------------------------------------

	public static bool GetClickedPixel(Vector3 _position, Camera _camera, RectTransform _rectArea, Texture2D _texture, out int _x, out int _y)
	{
		Vector2 localCursor;

		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectArea.GetComponent<RectTransform>(), _position, _camera, out localCursor))
		{
			_x	=	_y	=	0;
			return	false;
		}

		var	clickedPosition	=	new Vector2
								(
									localCursor.x,
									_rectArea.rect.height+localCursor.y
								);

		_x	=	Mathf.RoundToInt
				(
					clickedPosition.x 
					/ _rectArea.rect.width 
					* _texture.width
				);
		_y	=	Mathf.RoundToInt
				(
					clickedPosition.y
					/ _rectArea.rect.height 
					* _texture.height
				);
		return true;
	}

	//-----------------------------------------------------------------
}