using UnityEngine;

public class Singleton<T> : MonoBehaviour
{
	//-----------------------------------------------------------------

	public static T Instance;
	
	//-----------------------------------------------------------------

	private void Awake()
	{
		Instance    =   this.GetComponent<T>();
		AfterAwake();
	}
	
	//-----------------------------------------------------------------
	
	protected virtual void AfterAwake()
	{
		
	}
	
	//-----------------------------------------------------------------
}