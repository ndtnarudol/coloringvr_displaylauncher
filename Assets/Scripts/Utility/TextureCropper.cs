using UnityEngine;

public class TextureCropper
{
	//------------------------------------------------------------------------------------

	public static Texture2D Crop(Texture2D _texture, int _x, int _y, int _width, int _height)
	{
		var	resultTexture	=	new Texture2D
								(
									_width,
									_height,
									_texture.format,
									false
								);
		var	readPixel		=	_texture.GetPixels(_x, _y, _width, _height);
		resultTexture.SetPixels(readPixel);
		resultTexture.Apply();

		return resultTexture;
	}

	//------------------------------------------------------------------------------------
}