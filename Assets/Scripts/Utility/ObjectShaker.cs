﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectShaker : MonoBehaviour 
{
    //-----------------------------------------------------------------

    public static List<ObjectShaker> Instances  =   new List<ObjectShaker>();  
    
    //-----------------------------------------------------------------

	private	Vector3 forcePower;
    private Vector3 positionTemp;

    //-----------------------------------------------------------------

    private void Awake ()
    {
        Instances.Add(this);
    }

    //-----------------------------------------------------------------

    private void Start ()
    {
        positionTemp = this.transform.position;
    }
    
    //-----------------------------------------------------------------
	
	private void Update () 
    {
        forcePower = Vector3.Lerp (forcePower, Vector3.zero, Time.deltaTime * 5);	
        this.transform.position = positionTemp + new Vector3 (Mathf.Cos (Time.time * 80) * forcePower.x, Mathf.Cos (Time.time * 80) * forcePower.y, Mathf.Cos (Time.time * 80) * forcePower.z);
	}
    
    //-----------------------------------------------------------------

    public static void Shake(Vector3 power)
    {
        for(int i = 0; i < Instances.Count; i++)
        {
            Instances[i].forcePower = -power;
        }
    }    
    
    //-----------------------------------------------------------------
}
