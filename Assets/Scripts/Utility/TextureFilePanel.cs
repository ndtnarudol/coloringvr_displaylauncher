using UnityEngine;
using System;
using System.Collections;

#if !UNITY_EDITOR
using System.IO;
using System.Windows.Forms;
#else
using UnityEditor;
#endif

public class TextureFilePanel : Singleton<TextureFilePanel>
{
	//-----------------------------------------------------------------

	public static Texture2D Open(string _title, string _path, string _format)
	{
		var	result		=	new Texture2D(1,1);
		
		string		photoPath	=	string.Empty;

		#if UNITY_EDITOR
		
		photoPath	=	EditorUtility.OpenFilePanel
							(
								_title,
								_path,
								_format
							);

		#else

		OpenFileDialog openFileDialog1 = new OpenFileDialog();

		openFileDialog1.Title = _title;
		openFileDialog1.InitialDirectory = _path ;
		openFileDialog1.Filter =_format ;
		openFileDialog1.FilterIndex = 2 ;
		openFileDialog1.RestoreDirectory = true ;


		try
		{
			if(openFileDialog1.ShowDialog() == DialogResult.OK)
				photoPath	=	openFileDialog1.FileName;
		}
		catch(System.Exception _exception)
		{
			UIController.Instance.ShowWarningPopup
			(
				_title: 		"Save Path Unavailable",
				_message:		"Please setup save data location first."
			);
			Debug.Log("Unable to loading image\n" + _exception.ToString());
		}

		#endif

		if( photoPath.Length != 0 )
		{
			WWW www = new WWW( "file:///" + photoPath );
			www.LoadImageIntoTexture( result );
			www.Dispose();
		}
		else
		{
			result	=	null;
		}

		return result;
	}

	//-----------------------------------------------------------------

	public void Load(string _title, string _path, string _format, Action<Texture2D> _onLoaded)
	{
		StartCoroutine(OpenCoroutine(_title, _path, _format, _onLoaded));
	}

	//-----------------------------------------------------------------

	private IEnumerator OpenCoroutine(string _title, string _path, string _format, Action<Texture2D> _onLoaded)
	{
		Texture2D	result;
		string		photoPath	=	string.Empty;

		#if UNITY_EDITOR

		photoPath	=	EditorUtility.OpenFilePanel
							(
								_title,
								_path,
								_format
							);

		#else

		OpenFileDialog openFileDialog = new OpenFileDialog();

		openFileDialog.Title = _title;
		openFileDialog.InitialDirectory = _path ;
		openFileDialog.Filter =_format ;
		openFileDialog.FilterIndex = 2 ;
		openFileDialog.RestoreDirectory = true ;

		try
		{
			if(openFileDialog.ShowDialog() == DialogResult.OK)
				photoPath	=	openFileDialog.FileName;
		}
		catch(System.Exception _exception)
		{
			UIController.Instance.ShowWarningPopup
			(
				_title: 		"Save Path Unavailable",
				_message:		"Please setup save data location first."
			);
			Debug.Log("Unable to loading image\n" + _exception.ToString());
		}

		#endif

		if( photoPath.Length != 0 )
		{
			WWW www = new WWW( "file:///" + photoPath );
			yield return www;
			result	=	www.texture;
			if(_onLoaded != null) _onLoaded(result);
			www.Dispose();
		}
		else
		{
			result	=	null;
			if(_onLoaded != null) _onLoaded(result);
		}
	}

	//-----------------------------------------------------------------
} 