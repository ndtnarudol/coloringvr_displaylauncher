﻿using UnityEngine;

public class FlipCamera : MonoBehaviour
{
    public bool flipX = false;
    public bool flipY = false;
    public bool flipZ = false;

    void OnPreCull()
    {
        float fx = (flipX) ? -1 : 1;
        float fy = (flipY) ? -1 : 1;
        float fz = (flipZ) ? -1 : 1;

        GetComponent<Camera>().ResetWorldToCameraMatrix();
        GetComponent<Camera>().ResetProjectionMatrix();
        GetComponent<Camera>().projectionMatrix = GetComponent<Camera>().projectionMatrix * Matrix4x4.Scale(new Vector3(fx, fy, fz));
    }

    void OnPreRender()
    {
        // GL.SetRevertBackfacing(true);
    }

    void OnPostRender()
    {
        // GL.SetRevertBackfacing(false);
    }
}
