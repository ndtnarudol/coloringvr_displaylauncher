﻿using System.IO;

public class ConfigWriter
{
    
	//-----------------------------------------------------------------------------------

    private const string    FORMAT_SAVEDATA     =   "{0}={1}";

	//-----------------------------------------------------------------------------------

    public static Config WriteDefaultConfig()
    {
        UnityEngine.Debug.Log("Write default config.");

        Config result  =   new Config();
        string path = Directory.GetCurrentDirectory() + @"\config.txt";
		Directory.CreateDirectory(Path.GetDirectoryName(path));

		var writer = File.CreateText(path);

        if (File.Exists(path))
        {
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenPosX, result.screenPosX);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenPosY, result.screenPosY);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenWidth, result.screenWidth);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenHeight, result.screenHeight);

            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.Borderless, result.borderless);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.Moveable, result.moveable);

            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.Extensions, string.Join(",", result.extension));

            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ImagePath, result.imagePath);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ApplicationPath, result.applicationPath);
        }

        writer.Close();

        return result;
    }
	//-----------------------------------------------------------------------------------

    public void WriteConfig(Config _config)
    {
		string path = Directory.GetCurrentDirectory() + @"\config.txt";
		Directory.CreateDirectory(Path.GetDirectoryName(path));

		var writer = File.CreateText(path);

        if (File.Exists(path))
        {
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenPosX, _config.screenPosX);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenPosY, _config.screenPosY);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenWidth, _config.screenWidth);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ScreenHeight, _config.screenHeight);

            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.Borderless, _config.borderless);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.Moveable, _config.moveable);

            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.Extensions, string.Join(",", _config.extension));
            
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ImagePath, _config.imagePath);
            writer.WriteLine(FORMAT_SAVEDATA, Config.Key.ApplicationPath, _config.applicationPath);
        }

        writer.Close();
    }

	//-----------------------------------------------------------------------------------
}
