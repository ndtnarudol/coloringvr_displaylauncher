﻿using System.IO;

public class Config
{
    //-----------------------------------------------------------------------------------

    public enum Key
    {
        Borderless,
        Moveable,
        ImagePath,
        ApplicationPath,
        ScreenWidth,
        ScreenHeight,
        ScreenPosX,
        ScreenPosY,
        Extensions
    }

    //-----------------------------------------------------------------------------------

    private const       int         DEFAULT_SCREENPOSX      =   0;
    private const       int         DEFAULT_SCREENPOSY      =   0;
    private const       int         DEFAULT_SCREENWIDTH     =   1280;
    private const       int         DEFAULT_SCREENHEIGHT    =   720;

    private const       bool        DEFAULT_BORDERLESS      =   true;
    private const       bool        DEFAULT_MOVEABLE        =   true;
    private readonly    string[]    DEFAULT_EXTENSION       =   { "*.png" };


    private readonly    string      DEFAULT_IMAGEPATH       =   @"..\ImageGame\";
    private readonly    string      DEFAULT_APPLICATIONPATH =   "ColoringVR.exe";

    //-----------------------------------------------------------------------------------

    // Resolution
    public int      screenPosX;
    public int      screenPosY;
    public int      screenWidth;
    public int      screenHeight;

	//-----------------------------------------------------------------------------------

    // Other settings
    public bool     borderless;
    public bool     moveable;
    public string[] extension;

	//-----------------------------------------------------------------------------------

    // Path
    public string   imagePath;
    public string   applicationPath;

	//-----------------------------------------------------------------------------------

    public Config()
    {
        screenPosX      =   DEFAULT_SCREENPOSX;
        screenPosY      =   DEFAULT_SCREENPOSY;
        screenWidth     =   DEFAULT_SCREENWIDTH;
        screenHeight    =   DEFAULT_SCREENHEIGHT;

        borderless      =   DEFAULT_BORDERLESS;
        moveable        =   DEFAULT_MOVEABLE;

        extension       =   DEFAULT_EXTENSION;

        imagePath       =   Path.GetFullPath(DEFAULT_IMAGEPATH);
        applicationPath =   Path.Combine(Directory.GetCurrentDirectory(), DEFAULT_APPLICATIONPATH);
    }
	//-----------------------------------------------------------------------------------

    public Config   (
                        int         _screenPosX,
                        int         _screenPosY,
                        int         _screenWidth,
                        int         _screenHeight,
                        bool        _borderless,
                        bool        _moveable, 
                        string[]    _extension,   
                        string      _imagePath,   
                        string      _applicationPath
                    )
    {
        screenPosX      =   _screenPosX;
        screenPosY      =   _screenPosY;
        screenWidth     =   _screenWidth;
        screenHeight    =   _screenHeight;

        borderless      =   _borderless;
        moveable        =   _moveable;

        extension       =   _extension;

        imagePath       =   _imagePath;
        applicationPath =   _applicationPath;
    }

	//-----------------------------------------------------------------------------------
}
