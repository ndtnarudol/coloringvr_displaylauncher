﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;


public class ConfigReader
{
	//-------------------------------------------------------------------------

	private string path = "config.txt";

	//-------------------------------------------------------------------------

	public string value(Config.Key _key)
	{
		return value(_key.ToString());
	}

	//-------------------------------------------------------------------------

    public bool IsHaveConfig()
    {
        var result = true;
		try
		{
			string[] lines = File.ReadAllLines(path);
		}
		catch
		{
			Debug.Log("Config file is not found.");
		    result = false;
		}

		return result;
    }

	//-------------------------------------------------------------------------

    public Config GetConfig()
    {
        var result  =   new Config();

        try
        {
            if (IsHaveConfig())
            {
                int.TryParse(value(Config.Key.ScreenPosX),          out result.screenPosX);
                int.TryParse(value(Config.Key.ScreenPosY),          out result.screenPosY);
                int.TryParse(value(Config.Key.ScreenWidth),         out result.screenWidth);
                int.TryParse(value(Config.Key.ScreenHeight),        out result.screenHeight);

                bool.TryParse(value(Config.Key.Borderless),         out result.borderless);
                bool.TryParse(value(Config.Key.Moveable),           out result.moveable);

                result.extension        =   value(Config.Key.Extensions).Split(',');
                result.imagePath        =   value(Config.Key.ImagePath);
                result.applicationPath  =   value(Config.Key.ApplicationPath);
            }
            else
            {
                Debug.Log("Can't find config file");
                result  =   ConfigWriter.WriteDefaultConfig();
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            result  =   ConfigWriter.WriteDefaultConfig();
        }

        return result;
    }

	//-------------------------------------------------------------------------

	private string value(string key)
	{
		try
		{
			Dictionary<string, string> data = new Dictionary<string, string>();
			string[] lines = File.ReadAllLines(path);

			for (int i = 0; i < lines.Length; i++)
			{
				try
				{
					string[] d = lines[i].Split('=');
					data.Add(d[0], d[1]);
				}
				catch
				{
					Debug.Log("ConfigReader Parse data catch line > " + lines[i]);
				}
			}

			return data[key];
		}
		catch
		{
			throw ;
		}
	}

	//-------------------------------------------------------------------------
}

