﻿using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(Text))]
public class TextLocalizer : MonoBehaviour
{
    //-----------------------------------------------------------------

    private const string    LANGUAGE_KEY_ENGLISH        =   "en";
    private const string    LANGUAGE_KEY_JAPANESE       =   "ja";

    //-----------------------------------------------------------------

    [SerializeField] private string     m_LanguageKey;

    //-----------------------------------------------------------------

	private void Start ()
	{
	    var languageManager                 =   LanguageManager.Instance;
        languageManager.OnChangeLanguage    +=  OnLanguageChanged;
        
	    if (!languageManager.HasKey(m_LanguageKey))
	        GetComponent<Text>().text       =   languageManager.GetTextValue(m_LanguageKey);
	}

    //-----------------------------------------------------------------

	private void OnEnable ()
	{
	    var _manager                 =   LanguageManager.Instance;

	    if (!_manager.HasKey(m_LanguageKey))
	        return;

	    GetComponent<Text>().text   =   _manager.GetTextValue(m_LanguageKey);
	}

    //-----------------------------------------------------------------

	private void OnLanguageChanged (LanguageManager _manager)
	{
	    if (!_manager.HasKey(m_LanguageKey))
	        return;

	    GetComponent<Text>().text   =   _manager.GetTextValue(m_LanguageKey);
	}

    //-----------------------------------------------------------------
}
